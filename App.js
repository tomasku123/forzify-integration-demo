import React from 'react';
import {StyleSheet, Text, StatusBar, View, WebView, Dimensions} from 'react-native';
import Orientation from 'react-native-orientation';

export default class App extends React.Component {

    constructor() {
        super();
        this.state = {orientation: 'UNKNOWN'}
        this._onOrientationChange = this._onOrientationChange.bind(this);
    }

    _onOrientationChange(orientation) {
        console.log('New orientation: ' + orientation);
        this.setState(previousState => {
            return {orientation: orientation};
        });
    }

    componentDidMount() {
        console.log('componentDidMount()');

        Orientation.addOrientationListener(this._onOrientationChange);
    }

    componentWillUnmount() {
        Orientation.removeOrientationListener(this._onOrientationChange);
    }

    videoUrl = {uri: 'http://staging-eliteserien.forzasys.com/minified#/anxabtf2vhr4m'};

    // Assuming we are starting in portrait mode
    screenWidthPortrait = Dimensions.get('window').width;
    screenHeigthPortrait = Dimensions.get('window').height;

    render() {

        console.log('Height: ' + Dimensions.get('window').height +
            ' Width: ' + Dimensions.get('window').width +
            ' StatusBar.currentHeight: ' + StatusBar.currentHeight);

        if (this.state.orientation == 'LANDSCAPE') {
            return (
                <View style={styles.container}>
                    <View style={{
                        flex: 1,
                        left: 0,
                        right: 0,
                        height: this.screenHeigthPortrait / 16 * 9 - StatusBar.currentHeight
                    }}>
                        <WebView
                            source={this.videoUrl}
                            style={{
                                flex: 1
                            }}
                            allowsInlineMediaPlayback={true}
                            allowUniversalAccessFromFileURLs={true}
                            automaticallyAdjustContentInsets={false}
                            mediaPlaybackRequiresUserAction={false}
                            scrollEnabled={false}
                        />
                    </View>
                </View>
            );
        }

        return (
            <View style={styles.container}>
                <View style={{
                    height: this.screenWidthPortrait / 16 * 9,
                }}>
                    <WebView
                        source={this.videoUrl}
                        style={{
                            flex: 1
                        }}
                        allowsInlineMediaPlayback={true}
                        allowUniversalAccessFromFileURLs={true}
                        automaticallyAdjustContentInsets={false}
                        mediaPlaybackRequiresUserAction={false}
                        scrollEnabled={false}
                    />
                </View>
                <View style={styles.contentContainer}>
                    <Text style={styles.content}>Open up App.js to start working on your app!</Text>
                    <Text style={styles.content}>Changes you make will automatically reload.</Text>
                    <Text style={styles.content}>Shake your phone to open the developer menu.</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    contentContainer: {
        top: 0,
        flex: 1
    },
    content: {
        textAlign: 'center'
    }
});
